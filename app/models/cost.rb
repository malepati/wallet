class Cost < ActiveRecord::Base
  belongs_to :category, dependent: :destroy
  validates :amount,  numericality: { only_integer: true }
end
